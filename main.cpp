#include <iostream>
#include "main.h"

using namespace std;


accuracy resistance[] = {
        {0.1, 0.8, 3, 600},
        {1, 0.5, 2, 6000},
        {10, 0.5, 2, 60000},
        {100, 0.5, 2, 600000},
        {1000, 0.8, 2, 6000000},
        {10000, 1.2, 3, 60000000},
};

accuracy current[] = {
        {0.0000001, 0.5, 3, 0.0006},
        {0.000001, 0.5, 3, 0.006},
        {0.00001, 0.5, 3, 0.06},
        {0.0001, 0.8, 3, 0.6},
        {0.01, 1.2, 3, 10},
};

accuracy voltage[] = {
        {0.0001, 0.6, 2, 0.6},
        {0.001, 0.3, 2, 6},
        {0.01, 0.3, 2, 60},
        {0.1, 0.3, 2, 600},
        {1, 0.5, 3, 1000},
};

int main()
{
  while (true)
    {
        char progselect = 'a';
        cout << "Enter \"a\" to enter ANALOG mode\nEnter \"d\" to enter DIGITAL mode \n\nEnter\"e\" to exit\n";
        cin >> progselect;
        cin.clear();
        progselect = tolower(progselect);

        if (progselect == 'a') analog();
        else if (progselect == 'd') digital();
        else if (progselect == 'e') break;
    }

}

void digital()
{
    string unit[][2] = {
            {"Ohms", "Ohm"},
            {"Amperes", "A"},
            {"Volts", "V"},
    };
    string resolution[] = {
            "1 (0.1 Ohm), 2 (0.001 kOhm), 3 (0.01 kOhm), 4 (0.1 kOhm), 5 (0.001 MOhm), 6 (0.01 MOhm)",
            "1 (0.1uA), 2 (1uA), 3 (0.01mA), 4 (0.1mA), 5 (10mA)",
            "1 (0.1mV), 2 (0.001V), 3(0.01V), 4(0.1V), 5(1V)",
    };


    while (true) {
        cout << "Voltage = v, Amperage = a, Resistance = r: ";
        string choice;
        cin.sync();
        cin >> choice;

        accuracy *accuracy_table;
        int mode;

        if (choice == "r")
        {
            accuracy_table = resistance;
            mode = 0;
        }
        else if (choice == "a")
        {
            accuracy_table = current;
            mode = 1;
        }
        else if (choice == "v")
        {
            accuracy_table = voltage;
            mode = 2;
        }
        else
        {
            cout << "Mode does not exist!" << endl;
            continue;
        }

        while(true) {
            calculate_digital(unit[mode], resolution[mode], accuracy_table);

            cout << "\nPress e to go back to unit selection, enter anything else to continue\n";
            string exit;
            cin.sync();
            cin >> exit;
            if (exit == "e")
            {
                break;
            }
        }

        cout << "\nPress e to go back to mode selection, enter anything else to continue\n";
        string exit;
        cin.sync();
        cin >> exit;

        if (exit == "e")
        {
            break;
        }
    }
}

void calculate_digital(string *unit, string &resolution, accuracy *accuracy_table) {
    cout << "Enter X (in " << unit[0] << "): ";
    double x = 0;
    cin >> x;
    cin.clear();

    cout << "Choose resolution. Possible options: \n" << resolution << "\n";
    int choice;
    cin >> choice;
    cin.clear();

    accuracy accuracy = accuracy_table[choice - 1];

    double DeltaX = (accuracy.a * x / 100) + (accuracy.n * accuracy.resolution);
    double deltaX = DeltaX / x * 100; // %

    cout << endl
    << x << unit[1] << " | "
    << accuracy.resolution << unit[1] << " | "
    << accuracy.range << unit[1] << " | "
    << "+- (" << accuracy.a << "% +" << accuracy.n << ") | "
    << DeltaX << unit[1] << " | " << deltaX << "%" << endl;
}

void analog()
{
    char exit = 'a';
    double alpha = 0, alphamax = 0,xr = 0, x = 0, DeltaX = 0, deltaX =0, cl = 0;

    while (exit != 'e')
    {
        cout << "Enter accuracy class (specified on device): ";
        cin >> cl;
        cin.clear();
        cout << "Enter alpha:";
        cin >> alpha;
        cin.clear();
        cout << "Enter alphaMAX:";
        cin >> alphamax;
        cin.clear();
        cout << "Enter Xr: ";
        cin >> xr;
        cin.clear();

        x = alpha / alphamax * xr;
        DeltaX = cl * xr / 100;
        deltaX = DeltaX / x * 100; //%

        cout << "\n| " << alpha << " | " << alphamax << " | " << xr  << " | " << x << " | " << DeltaX << " | " << deltaX << "%" << endl;
        cout << "\nPress anything to continue, press e to exit\n";
        cin.sync();
        cin >> exit;
        exit = tolower(exit);
    }
}
