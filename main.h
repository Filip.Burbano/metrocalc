#ifndef METROCALC_MAIN_H
#define METROCALC_MAIN_H

using namespace std;

struct accuracy {
    double resolution;
    double a;
    double n;
    double range;
};

void digital();
void analog();
void calculate_digital(string *unit, string &resolution, accuracy *accuracy_table);


#endif //METROCALC_MAIN_H
